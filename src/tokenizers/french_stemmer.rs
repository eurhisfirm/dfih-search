// #![cfg_attr(feature = "cargo-clippy", allow(clippy::new_without_default))]

// Module adapted from https://github.com/tantivy-search/tantivy/blob/master/src/tokenizer/stemmer.rs

use rust_stemmers::{self, Algorithm};
use std::sync::Arc;
use tantivy::tokenizer::{Token, TokenFilter, TokenStream};

/// `FrenchStemmer` token filter.
/// Tokens are expected to be slugified beforehands.
#[derive(Clone)]
pub struct FrenchStemmer {
    stemmer_algorithm: Arc<Algorithm>,
}

impl FrenchStemmer {
    /// Creates a new FrenchStemmer `TokenFilter`.
    pub fn new() -> FrenchStemmer {
        FrenchStemmer {
            stemmer_algorithm: Arc::new(Algorithm::French),
        }
    }
}

impl<TailTokenStream> TokenFilter<TailTokenStream> for FrenchStemmer
where
    TailTokenStream: TokenStream,
{
    type ResultTokenStream = FrenchStemmerTokenStream<TailTokenStream>;

    fn transform(&self, token_stream: TailTokenStream) -> Self::ResultTokenStream {
        let inner_stemmer = rust_stemmers::Stemmer::create(Algorithm::French);
        FrenchStemmerTokenStream::wrap(inner_stemmer, token_stream)
    }
}

pub struct FrenchStemmerTokenStream<TailTokenStream>
where
    TailTokenStream: TokenStream,
{
    tail: TailTokenStream,
    stemmer: rust_stemmers::Stemmer,
}

impl<TailTokenStream> TokenStream for FrenchStemmerTokenStream<TailTokenStream>
where
    TailTokenStream: TokenStream,
{
    fn token(&self) -> &Token {
        self.tail.token()
    }

    fn token_mut(&mut self) -> &mut Token {
        self.tail.token_mut()
    }

    fn advance(&mut self) -> bool {
        if self.tail.advance() {
            // TODO remove allocation.
            let stemmed_str: String = self.stemmer.stem(&self.token().text).into_owned();
            self.token_mut().text.clear();
            self.token_mut().text.push_str(&stemmed_str);
            true
        } else {
            false
        }
    }
}

impl<TailTokenStream> FrenchStemmerTokenStream<TailTokenStream>
where
    TailTokenStream: TokenStream,
{
    fn wrap(
        stemmer: rust_stemmers::Stemmer,
        tail: TailTokenStream,
    ) -> FrenchStemmerTokenStream<TailTokenStream> {
        FrenchStemmerTokenStream { tail, stemmer }
    }
}
