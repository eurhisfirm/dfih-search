// Module adapted from https://github.com/tantivy-search/tantivy/blob/master/src/tokenizer/lower_caser.rs

use slug::slugify;
use std::mem;
use tantivy::tokenizer::{Token, TokenFilter, TokenStream};

/// Token filter that replace terms with their slugs.
#[derive(Clone)]
pub struct Slugifier;

impl<TailTokenStream> TokenFilter<TailTokenStream> for Slugifier
where
    TailTokenStream: TokenStream,
{
    type ResultTokenStream = SlugifierTokenStream<TailTokenStream>;

    fn transform(&self, token_stream: TailTokenStream) -> Self::ResultTokenStream {
        SlugifierTokenStream::wrap(token_stream)
    }
}

pub struct SlugifierTokenStream<TailTokenStream> {
    buffer: String,
    tail: TailTokenStream,
}

// Write a slugified version of text into output.
fn to_slug(text: &mut String, output: &mut String) {
    output.clear();
    output.push_str(&slugify(text).replace("-", " "));
}

impl<TailTokenStream> TokenStream for SlugifierTokenStream<TailTokenStream>
where
    TailTokenStream: TokenStream,
{
    fn token(&self) -> &Token {
        self.tail.token()
    }

    fn token_mut(&mut self) -> &mut Token {
        self.tail.token_mut()
    }

    fn advance(&mut self) -> bool {
        if self.tail.advance() {
            to_slug(&mut self.tail.token_mut().text, &mut self.buffer);
            mem::swap(&mut self.tail.token_mut().text, &mut self.buffer);
            true
        } else {
            false
        }
    }
}

impl<TailTokenStream> SlugifierTokenStream<TailTokenStream>
where
    TailTokenStream: TokenStream,
{
    fn wrap(tail: TailTokenStream) -> SlugifierTokenStream<TailTokenStream> {
        SlugifierTokenStream {
            tail,
            buffer: String::with_capacity(100),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Slugifier;
    use tantivy::tokenizer::SimpleTokenizer;
    use tantivy::tokenizer::TokenStream;
    use tantivy::tokenizer::Tokenizer;

    #[test]
    fn test_to_slug() {
        assert_eq!(
            slug_helper("Русский текст"),
            vec!["русский".to_string(), "текст".to_string()]
        );
    }

    fn slug_helper(text: &str) -> Vec<String> {
        let mut tokens = vec![];
        let mut token_stream = SimpleTokenizer.filter(Slugifier).token_stream(text);
        while token_stream.advance() {
            let token_text = token_stream.token().text.clone();
            tokens.push(token_text);
        }
        tokens
    }

    #[test]
    fn test_slugr() {
        assert_eq!(slug_helper("Tree"), vec!["tree".to_string()]);
        assert_eq!(slug_helper("Русский"), vec!["русский".to_string()]);
    }
}
