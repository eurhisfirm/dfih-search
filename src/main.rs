#[macro_use]
extern crate clap;
extern crate json;
extern crate rust_stemmers;
extern crate serde;
extern crate serde_derive;
extern crate serde_json;
extern crate slug;
extern crate tantivy;
#[macro_use]
extern crate tower_web;

mod commands;
mod issuers;
mod persons;
mod tokenizers;

use clap::{App, AppSettings, Arg, SubCommand};
use commands::*;
use std::io::Write;

fn main() {
    let data_dir_arg = Arg::with_name("data_dir")
        .short("d")
        .long("data_dir")
        .value_name("data_dir")
        .help("Directory containing the DFIH data")
        .default_value("../dfih-ui/data");

    let cli_options = App::new("DFIH-Search")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .version(env!("CARGO_PKG_VERSION"))
        .author(crate_authors!())
        .about(r#"Search web service for the "Données financières historiques (DFIH)" project"#)
        .subcommand(
            SubCommand::with_name("index")
                .about("Index DFIH JSON files")
                .arg(data_dir_arg.clone()),
        )
        .subcommand(
            SubCommand::with_name("serve")
                .about("Start web server")
                .arg(data_dir_arg.clone())
                .arg(
                    Arg::with_name("host")
                        .long("host")
                        .value_name("host")
                        .help("host to listen to")
                        .default_value("127.0.0.1"),
                )
                .arg(
                    Arg::with_name("port")
                        .short("p")
                        .long("port")
                        .value_name("port")
                        .help("Port")
                        .default_value("2999"),
                ),
        )
        .get_matches();

    let (subcommand, some_options) = cli_options.subcommand();
    let options = some_options.unwrap();
    let run_cli = match subcommand {
        "index" => run_index_cli,
        "serve" => run_serve_cli,
        _ => panic!("Subcommand {} is unknown", subcommand),
    };

    if let Err(ref e) = run_cli(options) {
        let stderr = &mut std::io::stderr();
        let errmsg = "Error writing ot stderr";
        writeln!(stderr, "{}", e).expect(errmsg);
        std::process::exit(1);
    }
}
