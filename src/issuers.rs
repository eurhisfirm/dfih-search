use slug::slugify;
use std::fs;
use std::path::Path;
use tantivy::schema::*;
use tantivy::tokenizer::{RemoveLongFilter, SimpleTokenizer, StopWordFilter, Tokenizer};
use tantivy::Index;
use tokenizers::{FrenchStemmer, NgramTokenizer, Slugifier, FRENCH_STOP_WORDS};

pub fn build_index(data_dir: &Path) -> tantivy::Result<tantivy::Index> {
    let index_dir = data_dir.join("indexes").join("issuers");
    fs::create_dir_all(&index_dir).expect("Directory creation failed");
    let mut schema_builder = SchemaBuilder::default();

    schema_builder.add_i64_field("id", STORED);
    schema_builder.add_text_field("main_name", STORED);

    {
        let text_field_indexing = TextFieldIndexing::default()
            .set_tokenizer("french_tokenizer")
            .set_index_option(IndexRecordOption::WithFreqsAndPositions);
        let text_options = TextOptions::default()
            .set_indexing_options(text_field_indexing)
            .set_stored();
        schema_builder.add_text_field("name", text_options);
    }

    schema_builder.add_u64_field("prices_count", FAST);

    schema_builder.add_u64_field("public_status", INDEXED);

    let schema = schema_builder.build();
    let index = Index::create_in_dir(&index_dir, schema.clone()).unwrap();

    register_tokenizers(&index)?;

    Ok(index)
}

pub fn load_index(data_dir: &Path) -> tantivy::Result<tantivy::Index> {
    let index_dir = data_dir.join("indexes").join("issuers");
    let index = Index::open_in_dir(index_dir)?;
    register_tokenizers(&index)?;
    Ok(index)
}

fn register_tokenizers(index: &tantivy::Index) -> tantivy::Result<()> {
    let tokenizers = index.tokenizers();

    let french_autocomplete_tokenizer = NgramTokenizer::new(1, 3, false).filter(Slugifier);
    tokenizers.register(
        "french_autocomplete_tokenizer",
        french_autocomplete_tokenizer,
    );

    let french_tokenizer = SimpleTokenizer
        .filter(RemoveLongFilter::limit(40))
        .filter(StopWordFilter::remove(
            FRENCH_STOP_WORDS.iter().map(slugify).collect(),
        ))
        .filter(Slugifier)
        .filter(FrenchStemmer::new());
    tokenizers.register("french_tokenizer", french_tokenizer);

    Ok(())
}
