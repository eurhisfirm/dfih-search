use clap::ArgMatches;
use issuers;
use persons;
use std::convert::From;
use std::net::SocketAddr;
use std::path::Path;
use tantivy;
use tantivy::collector::{Count, TopDocs};
use tantivy::query::{BooleanQuery, Occur, QueryParser, RangeQuery};
use tantivy::schema::*;
use tantivy::DocAddress;
use tantivy::Document;
use tantivy::{doc, Index, ReloadPolicy};
use tower_web::middleware::cors::{AllowedOrigins, CorsBuilder};
use tower_web::{Response, ServiceBuilder};

#[derive(Serialize)]
struct Hit {
    doc: NamedFieldDocument,
    id: u32,
}

struct IssuersResource {
    index: Index,
    list_query_parser: QueryParser,
    schema: Schema,
}

impl IssuersResource {
    fn create_hit(&self, doc: &Document, doc_address: &DocAddress) -> Hit {
        Hit {
            doc: self.schema.to_named_doc(&doc),
            id: doc_address.doc(),
        }
    }
}

#[derive(Debug, Deserialize, Extract)]
struct IssuersSearchQuery {
    #[serde(default = "limit_default")]
    limit: usize,
    #[serde(default)]
    q: String,
    #[serde(default)]
    offset: usize,
    #[serde(default = "public_status_default")]
    public_status: usize,
}

struct PersonsResource {
    index: Index,
    list_query_parser: QueryParser,
    schema: Schema,
}

impl PersonsResource {
    fn create_hit(&self, doc: &Document, doc_address: &DocAddress) -> Hit {
        Hit {
            doc: self.schema.to_named_doc(&doc),
            id: doc_address.doc(),
        }
    }
}

#[derive(Response)]
struct ListResponse {
    q: String,
    count: usize,
    hits: Vec<Hit>,
}

#[derive(Debug, Deserialize, Extract)]
struct SearchQuery {
    #[serde(default = "limit_default")]
    limit: usize,
    #[serde(default)]
    q: String,
    #[serde(default)]
    offset: usize,
}

fn limit_default() -> usize {
    10
}

fn public_status_default() -> usize {
    2
}

impl_web! {
    impl IssuersResource {

        #[get("/issuers")]
        #[content_type("application/json")]
        fn list(&self, query_string: IssuersSearchQuery) -> Result<ListResponse, ()> {
            let reader = self.index
            .reader_builder()
            .reload_policy(ReloadPolicy::OnCommit)
            .try_into()
            .unwrap();

            let searcher = reader.searcher();
            let schema = self.index.schema();
            let prices_count_field = schema.get_field("prices_count").unwrap();
            let public_status = query_string.public_status;

            let q = query_string.q.replace("'", " ");
            let q = q.replace("\"", " ");
            let q = q.replace(":" , " ");
            let q = q.replace("-" , " ");
            let q = q.replace("+" , " ");
            let q = q.trim();
            let q = if q.is_empty() {
                "*"
            } else {
                q
            };

            let term_query = self.list_query_parser
                .parse_query(&q)
                .expect("Parsing the query failed");

            let range = if public_status < 2 {
                public_status as u64..public_status as u64 + 1
            } else {
                0..2
            };

            let public_status_query = RangeQuery::new_u64(self.schema.get_field(
                            "public_status").unwrap(),
                            range,
                        );
            let query = BooleanQuery::from(vec![
                            (Occur::Must, term_query),
                            (Occur::Must, Box::new(public_status_query)),
                        ]);

            let (top_docs, doc_count) = if q == "*" {
                let (top_docs_u64, doc_count) = searcher.search
                        (
                        &query,
                        &(TopDocs
                            ::with_limit(query_string.offset + query_string.limit)
                            .order_by_u64_field(prices_count_field), Count)
                        )
                        .unwrap();
                (
                top_docs_u64
                .iter()
                .map(|(score, doc_addr)| (*score as f32, *doc_addr))
                .collect(),
                doc_count
                )
            }
            else {searcher.search(&query, &(TopDocs::with_limit(query_string.offset + query_string.limit), Count)).unwrap()};

            let hits: Vec<Hit> = {
                top_docs
                .iter()
                .skip(query_string.offset)
                .map(|(_score, doc_address)| {
                    let doc: Document = searcher.doc(*doc_address).unwrap();
                    self.create_hit(&doc, doc_address)
                })
                .collect()
            };
            Ok(ListResponse {
                q: query_string.q.clone(),
                count: doc_count,
                hits,
            })
        }
    }

    impl PersonsResource {
        #[get("/persons")]
        #[content_type("application/json")]
        fn list(&self, query_string: SearchQuery) -> Result<ListResponse, ()> {
            let reader = self.index
            .reader_builder()
            .reload_policy(ReloadPolicy::OnCommit)
            .try_into()
            .unwrap();

            let searcher = reader.searcher();
            let schema = self.index.schema();
            let functions_count_field = schema.get_field("functions_count").unwrap();

            let q = query_string.q.replace("'", " ");
            let q = q.replace("\"", " ");
            let q = q.replace(":" , " ");
            let q = q.replace("-" , " ");
            let q = q.replace("+" , " ");
            let q = q.trim();
            let q = if q.is_empty() {
                "*"
            } else {
                q
            };

            let query = self.list_query_parser
                .parse_query(&q)
                .expect("Parsing the query failed");

            let (top_docs, doc_count) = if q == "*" {
                let (top_docs_u64, doc_count) = searcher.search
                        (
                        &query,
                        &(TopDocs
                            ::with_limit(query_string.offset + query_string.limit)
                            .order_by_u64_field(functions_count_field), Count)
                        )
                        .unwrap();
                (
                top_docs_u64
                .iter()
                .map(|(score, doc_addr)| (*score as f32, *doc_addr))
                .collect(),
                doc_count
                )
            }
            else {searcher.search(&query, &(TopDocs::with_limit(query_string.offset + query_string.limit), Count)).unwrap()};

            let hits: Vec<Hit> = {
                top_docs
                .iter()
                .skip(query_string.offset)
                .map(|(_score, doc_address)| {
                    let doc: Document = searcher.doc(*doc_address).unwrap();
                    self.create_hit(&doc, doc_address)
                })
                .collect()
            };
            Ok(ListResponse {
                q: query_string.q.clone(),
                count: doc_count,
                hits,
            })
        }
    }
}

fn run_serve(data_dir: &Path, addr: &SocketAddr) -> tantivy::Result<()> {
    let issuers_resource = {
        let index = issuers::load_index(data_dir)?;
        let schema = index.schema();
        let name_field = schema.get_field("name").unwrap();
        let list_query_parser = QueryParser::for_index(&index, vec![name_field]);
        IssuersResource {
            index,
            list_query_parser,
            schema,
        }
    };

    let persons_resource = {
        let index = persons::load_index(data_dir)?;
        let schema = index.schema();
        let name_field = schema.get_field("name").unwrap();
        let list_query_parser = QueryParser::for_index(&index, vec![name_field]);
        PersonsResource {
            index,
            list_query_parser,
            schema,
        }
    };

    let cors = CorsBuilder::new()
        .allow_origins(AllowedOrigins::Any { allow_null: true })
        .build();

    println!("Listening on http://{}", addr);
    ServiceBuilder::new()
        .resource(issuers_resource)
        .resource(persons_resource)
        .middleware(cors)
        .run(addr)
        .unwrap();
    Ok(())
}

pub fn run_serve_cli(args: &ArgMatches) -> Result<(), String> {
    let data_dir = args
        .value_of("data_dir")
        .map(|path| Path::new(path))
        .expect("Invalid data directory");
    let port = value_t!(args, "port", u16).unwrap();
    let host = args.value_of("host").unwrap();
    let addr = format!("{}:{}", host, port)
        .parse()
        .expect("Invalid address");
    run_serve(data_dir, &addr).map_err(|e| format!("{:?}", e))
}
