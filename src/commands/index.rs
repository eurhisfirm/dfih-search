use clap::ArgMatches;
use issuers;
use persons;
use std::fs::{self, File};
use std::io::prelude::*;
use std::path::Path;
use tantivy::schema::Document;

fn run_index(data_dir: &Path) -> tantivy::Result<()> {
    {
        let index = issuers::build_index(data_dir)?;
        let mut index_writer = index.writer(50_000_000)?;
        let issuers_dir = data_dir.join("issuers");
        let schema = index.schema();

        let id_field = schema.get_field("id").unwrap();
        let main_name_field = schema.get_field("main_name").unwrap();
        let name_field = schema.get_field("name").unwrap();
        let prices_count_field = schema.get_field("prices_count").unwrap();
        let public_status_field = schema.get_field("public_status").unwrap();

        for entry in fs::read_dir(issuers_dir).unwrap() {
            let json_file_path = entry.unwrap().path();
            let mut json_file = File::open(json_file_path).expect("JSON file not found");
            let mut json_string = String::new();
            json_file
                .read_to_string(&mut json_string)
                .expect("An error occurred while reading file");
            let issuer = json::parse(&json_string).expect("Invalid JSON");
            let mut issuer_doc = Document::default();
            issuer_doc.add_u64(id_field, issuer["id"].as_u64().unwrap());
            issuer_doc.add_text(main_name_field, issuer["name"].as_str().unwrap());
            for name in issuer["names"].members() {
                issuer_doc.add_text(name_field, &name.to_string());
            }
            let prices_count: u64 = issuer["securities"]
                .entries()
                .map(|(_id, security)| security["prices_count"].as_u64().unwrap())
                .sum();

            issuer_doc.add_u64(prices_count_field, prices_count);

            let public_status_bool = match issuer["public_status"].as_bool().unwrap() {
                true => 1,
                false => 0,
            };
            issuer_doc.add_u64(public_status_field, public_status_bool);

            index_writer.add_document(issuer_doc);
        }

        index_writer.commit()?;
    }

    {
        let index = persons::build_index(data_dir)?;
        let mut index_writer = index.writer(50_000_000)?;
        let persons_dir = data_dir.join("persons");
        let schema = index.schema();

        let id_field = schema.get_field("id").unwrap();
        let main_name_field = schema.get_field("main_name").unwrap();
        let name_field = schema.get_field("name").unwrap();
        let functions_count_field = schema.get_field("functions_count").unwrap();

        for entry in fs::read_dir(persons_dir).unwrap() {
            let json_file_path = entry.unwrap().path();
            let mut json_file = File::open(json_file_path).expect("JSON file not found");
            let mut json_string = String::new();
            json_file
                .read_to_string(&mut json_string)
                .expect("An error occurred while reading file");
            let person = json::parse(&json_string).expect("Invalid JSON");
            let mut person_doc = Document::default();
            person_doc.add_u64(id_field, person["id"].as_u64().unwrap());
            person_doc.add_text(main_name_field, person["name"].as_str().unwrap());
            for name in person["names"].members() {
                person_doc.add_text(name_field, &name.to_string());
            }
            person_doc.add_u64(functions_count_field, person["functions"].len() as u64);
            index_writer.add_document(person_doc);
        }
        index_writer.commit()?;
    }

    Ok(())
}

pub fn run_index_cli(args: &ArgMatches) -> Result<(), String> {
    let data_dir = args
        .value_of("data_dir")
        .map(|path| Path::new(path))
        .unwrap();
    run_index(data_dir).map_err(|e| format!("Indexing failed : {:?}", e))
}
